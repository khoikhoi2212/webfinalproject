
//app/models/user.js
//load the things we need
var mongoose = require('mongoose');
var bcrypt   = require('bcrypt-nodejs');

//define the schema for our user model
var userSchema = mongoose.Schema({	
	fullname: String,
	mail: String,
	password: String,
	image: { data: Buffer, contentType: String },
	dateOfBirth: Date,
	address: String,
	status: String,
	otp: Number,
	role: { type: String, default: "Bidder" },
	productsBid: [{ type: mongoose.Schema.Types.ObjectId, ref: 'products' }],
	productsSel: [{ type: mongoose.Schema.Types.ObjectId, ref: 'products' }],
	productsWon: [{ type: mongoose.Schema.Types.ObjectId, ref: 'products' }],
	favoriteProducts: [{ type: mongoose.Schema.Types.ObjectId, ref: 'products' }],
	rated: [{
		date: { type: Date, default: Date.now },
		point: Number,
		user: { type: mongoose.Schema.Types.ObjectId, ref: 'users' },
		comment: String
	}],
	ratedPoint: { 
		type: String, 
		default: function() {
			if (!this.rated) {
				return "unrated";
			} else {
				var point = 0;
				for (var i=0; i<this.rated.length; i++) {
					point += this.rated[i].point;
				}
				return (point/this.rated.length)*100 + "%";
			}
		}
	}
});


//methods ======================
//generating a hash
userSchema.methods.generateHash = function(password) {
 return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

//checking if password is valid
userSchema.methods.validPassword = function(password) {
 return bcrypt.compareSync(password, this.password);
};

//create the model for users and expose it to our app
module.exports = mongoose.model('users', userSchema);
var mongoose = require('mongoose');

//define the schema for our user model
var productSchema = mongoose.Schema({	
	name: String,
	image: [{ data: Buffer, contentType: String }],
	startDate: { type: Date, default: Date.now },
	endDate: { type: Date, default: () => Date.now() + 7*24*60*60*1000},
	startPrice: Number,
	priceStep: Number,
	purchasePrice: Number,
	autoRenew: { type: Boolean, default: false },
	description: String,
	seller: { type: mongoose.Schema.Types.ObjectId, ref: 'users' },
	category: { type: mongoose.Schema.Types.ObjectId, ref: 'categories' },
	history: [{
		date: { type: Date, default: Date.now },
		bidder: { type: mongoose.Schema.Types.ObjectId, ref: 'users' },
		price: Number,
	}],
	present: {
		date: Date,
		bidder: { type: mongoose.Schema.Types.ObjectId, ref: 'users' },
		price: Number,
	}
});

productSchema.index({name: 'text', category: 'text'});


//methods ======================

//create the model for users and expose it to our app
module.exports = mongoose.model('products', productSchema);
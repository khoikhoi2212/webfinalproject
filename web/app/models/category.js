var mongoose = require('mongoose');


var categorySchema = mongoose.Schema({
	name : String,
	parent :  { type: mongoose.Schema.Types.ObjectId, ref: 'categories' }
});

categorySchema.index({name: 'text'});

// Các phương thức ======================

module.exports = mongoose.model('categories', categorySchema);
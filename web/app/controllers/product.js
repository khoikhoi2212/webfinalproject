var Product = require('../models/product');
var Category = require('../models/category');
var User = require('../models/user');
var moment = require('moment');
var fs = require('fs');
var path = require("path");
var async = require('async');

exports.deleteProduct = function(req, res) {
    Product.deleteOne({ _id: req.query.id }, function (err) {});
    res.redirect('/adminPage');
}

exports.productDetailPage = function(req, res) {
    var ProductObject = Product.findOne({ _id : req.query.id }).populate('seller').populate('category').populate('history.bidder').populate('present.bidder');
    var CategoryObjects = Category.find({}).populate('parent');
    var ThisUserObject = User.findOne({ _id : req.session.user});

    var resources = {
        categories: CategoryObjects.exec.bind(CategoryObjects),
        product: ProductObject.exec.bind(ProductObject),
        thisUser: ThisUserObject.exec.bind(ThisUserObject),
        moment: function(callback) {callback(null,moment);}
    };

    async.parallel(resources, function (error, results){
        if (error) {
            res.status(500).send(error);
            return;
        }
        res.render('productDetail.ejs',results);
    });
}

exports.productListByCategory = function(req, res) {
    /*var ProductObjects;
    var category = Category.findOne({ _id: req.params.id });
    if (category.parent == undefined) {
        var categories = Category.find({ 'category.parent': req.params.id });
        ProductObjects = Product.find({ 'category.parent': req.params.id });
    } else {*/
    //}
    var ProductObjects = Product.find({ category: req.params.id }).populate('seller').populate('category').populate('history.bidder').populate('present.bidder');

    var sortType = 1;

    if(req.query.sort == 2) {
        ProductObjects = ProductObjects.sort({ 'present.price': 1});
        sortType = 2;
    } else if (req.query.sort == 3) {
        ProductObjects = ProductObjects.sort({ 'present.price': -1});
        sortType = 3;
    } else {
        ProductObjects = ProductObjects.sort({ endDate: -1});
    }
    var page = 1;
    if (req.query.page) {
        page = req.query.page;
    }
    var ProductShowObjects = ProductObjects.limit(12).skip(12*(page-1));

    ProductObjects = Product.find({ category: req.params.id });

    var CategoryObjects = Category.find({}).populate('parent');
    var ThisUserObject = User.findOne({ _id : req.session.user});

    var resources = {
        products:  ProductObjects.exec.bind(ProductObjects),
        productsShow:  ProductShowObjects.exec.bind(ProductShowObjects),
        categories: CategoryObjects.exec.bind(CategoryObjects),
        sortType: function(callback) {callback(null,sortType);},
        page: function(callback) {callback(null,page);},
        categoryId:  function(callback) {callback(null,req.params.id);},
        thisUser: ThisUserObject.exec.bind(ThisUserObject),
        moment: function(callback) {callback(null,moment);}
    };

    async.parallel(resources, function (error, results){
        if (error) {
            res.status(500).send(error);
            return;
        }
        res.render('productListByCategory.ejs',results);
    });
}

exports.productList = function(req, res) {

    var ProductObjects = Product.find({}).populate('seller').populate('category').populate('history.bidder').populate('present.bidder');
    var sortType = 1;

    if(req.query.sort == 2) {
        ProductObjects = ProductObjects.sort({ 'present.price': 1});
        sortType = 2;
    } else if (req.query.sort == 3) {
        ProductObjects = ProductObjects.sort({ 'present.price': -1});
        sortType = 3;
    } else {
        ProductObjects = ProductObjects.sort({ endDate: -1});
    }
    var page = 1;
    if (req.query.page) {
        page = req.query.page;
    }
    var ProductShowObjects = ProductObjects.limit(12).skip(12*(page-1));

    ProductObjects = Product.find({});

    var CategoryObjects = Category.find({}).populate('parent');
    var ThisUserObject = User.findOne({ _id : req.session.user});

    var resources = {
        products:  ProductObjects.exec.bind(ProductObjects),
        productsShow:  ProductShowObjects.exec.bind(ProductShowObjects),
        categories: CategoryObjects.exec.bind(CategoryObjects),
        sortType: function(callback) {callback(null,sortType);},
        page: function(callback) {callback(null,page);},
        categoryId:  function(callback) {callback(null,"0");},
        thisUser: ThisUserObject.exec.bind(ThisUserObject),
        moment: function(callback) {callback(null,moment);}
    };

    async.parallel(resources, function (error, results){
        if (error) {
            res.status(500).send(error);
            return;
        }
        res.render('productList.ejs',results);
    });
}

exports.searchProduct =function (req, res) {
    Product.find( { $text: { $search: req.query.searchString } }).exec(function(err, docs) {
        var products =  docs;
        /*if (req.query.searchType == 2) {
            ProductObjects = Product.find( {$text: {$search: req.query.search} });
        }
        
        if(req.query.sort == 2) {
            ProductObjects = ProductObjects.sort({ 'present.price': 1});
        } else if (req.query.sort == 3) {
            ProductObjects = ProductObjects.sort({ 'present.price': -1});
        } else {
            ProductObjects = ProductObjects.sort({ endDate: -1});
        }*/

        var page = 1;
        if (req.query.page) {
            age = req.query.page;
        }
        var ProductShowObjects = Product.find( { $text: { $search: req.query.search } }).limit(12).skip(12*(page-1));

        var CategoryObjects = Category.find({}).populate('parent');
        var ThisUserObject = User.findOne({ _id : req.session.user});

        var resources = {
            products:  function(callback) {callback(null,products);},
            productsShow:  ProductShowObjects.exec.bind(ProductShowObjects),
            categories: CategoryObjects.exec.bind(CategoryObjects),
            sortType: function(callback) {callback(null,"0");},
            page: function(callback) {callback(null,page);},
            categoryId:  function(callback) {callback(null,"0");},
            thisUser: ThisUserObject.exec.bind(ThisUserObject),
            moment: function(callback) {callback(null,moment);}
        };

        async.parallel(resources, function (error, results){
            if (error) {
                res.status(500).send(error);
                return;
            }
            res.render('productList.ejs',results);
        });
    });

}

exports.productEditPage = function(req, res) {
    var CategoryObjects = Category.find({}).populate('parent');
    var ThisUserObject = User.findOne({ _id : req.session.user});
    var resources = {
        categories: CategoryObjects.exec.bind(CategoryObjects),
        thisUser: ThisUserObject.exec.bind(ThisUserObject),
        moment: function(callback) {callback(null,moment);}
    };

    async.parallel(resources, function (error, results){
        if (error) {
            res.status(500).send(error);
            return;
        }
        res.render('productEdit.ejs',results);
    });
}

exports.uploadCKeditor = function(req, res) {

    html = "";
    html += "<script type='text/javascript'>";
    html += "    var funcNum = " + req.query.CKEditorFuncNum + ";";
    html += "    var url =\"data:" + req.files.upload.type + ";base64," + fs.readFileSync(req.files.upload.path).toString('base64') + "\";";      
    html += "    var message = \"Uploaded file successfully"+" MotherFucker"+"\";";
    html += "";
    html += "    window.parent.CKEDITOR.tools.callFunction(funcNum, url, message);";
    html += "</script>";

    res.send(html);
}

exports.postProduct = function(req, res) {
    
    var newProduct = new Product();

    Category.findOne({ name: req.body.category } , function (err, doc) {
        newProduct.name = req.body.name;
        newProduct.category = doc._id;
        newProduct.startPrice = req.body.startPrice;
        newProduct.priceStep = req.body.priceStep;
        newProduct.purchasePrice = req.body.purchasePrice;
        newProduct.description = req.body.ckeditor;
        newProduct.autoRenew = req.body.autoRenew ? true : false;
        newProduct.seller = req.session.user._id;

        for (var i = 0; i < req.files.image.length; i++) {
            var productImage = { data: fs.readFileSync(req.files.image[i].path), contentType: req.files.image[i].type};
            newProduct.image.push(productImage);
        }
        
        newProduct.save();
    });

    User.findOne({ _id: req.session.user._id} , function (err, user) {
        user.productsSel.push(newProduct);
        user.save();
    });

    res.redirect('/');
}

exports.bidProduct = function(req, res) {
    Product.findOne({ _id: req.query.productId } ,function (err, doc) {
        var thisTime = new Date().getTime();
        if ((doc.endDate.getTime() - thisTime) > 5*60*1000) {
            var time = doc.endDate.getTime() + 10*60*1000;
            doc.endDate = new Date(time);
        }
        var newBid = { bidder: req.session.user._id, price: req.body.price, date: new Date() };
        doc.present = newBid;
        doc.history.push(newBid); 
        doc.save();
        User.findOne({ _id: req.session.user._id} , function (err, user) {
            user.productsBid.push(doc);
            user.save();
        });
    });
    res.redirect('/');
}

exports.purchase = function(req, res) {
    Product.findOne({ _id: req.query.productId } ,function (err, doc) {
        doc.endDate = new Date();
        doc.present.date = new Date();
        doc.present.bidder = req.session.user._id;
        doc.present.price = req.body.price;
        doc.save();
        User.findOne({ _id: req.session.user._id} , function (err, user) {
            user.productsWon.push(doc);
            user.save();
        });
    });
    res.redirect('/');
}
var Category = require('../models/category');

exports.addCategory = function(req, res) {
    if (req.body.parent != "Không") {
        Category.findOne({ name: req.body.parent } , function (err, doc) {
            var newCategories = new Category({
                name: req.body.category,
                parent: doc._id
            });
            newCategories.save();
        });
    } else {
        var newCategories = new Category({
            name: req.body.category,
            parent: undefined
        });
        newCategories.save();
    }
    res.redirect('/adminPage');
}

exports.deleteCategory = function(req, res) {
    Category.deleteOne({ _id: req.query.id }, function (err) {});
    res.redirect('/adminPage');
}

exports.editCategory = function(req, res) {
    var id;
    if (req.body.parent != "Không") {
        Category.findOne({ name: req.body.parent } , function (err, parent) {
            id = parent._id;
        });
    } else {
        id = undefined;
    }
    Category.findById(req.query.id , function (err, doc) {
        doc.name = req.body.category;
        doc.parent = id;
        doc.save();
    });
    res.redirect('/adminPage');
}
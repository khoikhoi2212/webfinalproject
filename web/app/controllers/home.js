var bcrypt = require('bcrypt-nodejs');
var async = require('async');
var moment = require('moment');
var Category = require('../models/category');
var User = require('../models/user');
var Product = require('../models/product');
var ObjectId = require('mongodb').ObjectID;

exports.adminLoggedIn = function(req, res, next)
{
    if (req.session.user.role == "Admin") { // req.session.passport._id

        next();

    } else {

        res.redirect('/');
    }
}

exports.home = function(req, res) {
    var ProductTopPriceObjects = Product.find({}).sort({ startPrice: -1 }).populate('present.bidder').limit(5);
    var ProductTopBidObjects = Product.find({}).sort({ history: -1 }).populate('present.bidder').limit(5);
    var ProductEndSoonObjects = Product.find({}).sort({ endDate: 1 }).populate('present.bidder').limit(5);
    var CategoryObjects = Category.find({}).populate('parent');
    var ThisUserObject = User.findOne({ _id : req.session.user});

    var resources = {
        productsTopPrice: ProductTopPriceObjects.exec.bind(ProductTopPriceObjects),
        productsTopBid: ProductTopBidObjects.exec.bind(ProductTopBidObjects),
        productsEndSoon: ProductEndSoonObjects.exec.bind(ProductEndSoonObjects),
        categories: CategoryObjects.exec.bind(CategoryObjects),
        thisUser: ThisUserObject.exec.bind(ThisUserObject),
        moment: function(callback) {callback(null,moment);}
    };

    async.parallel(resources, function (error, results){
        if (error) {
            res.status(500).send(error);
            return;
        }
        res.render('home.ejs',results);
    });  
}


exports.signup = function(req, res) {

    if (req.session.user) {

        res.redirect('/home');

    } else {

        res.render('signup.ejs', {
            error : req.flash("error"),
            success: req.flash("success"),
            session:req.session
        });
    }
}


exports.login = function(req, res) {

    if (req.session.user) {

        res.redirect('/home');

    } else {

        res.render('login.ejs', {
            error : req.flash("error"),
            success: req.flash("success"),
            session:req.session
        });
    }  
}

exports.changePassword = function(req, res) {
    res.render('changePassword.ejs',  {
        user : req.user 
    });
}

exports.logout = function (req, res) {
    req.logout();
    res.redirect('/');
}

var user = new User();

exports.otp = function(req, res) {
    user = User.findOne({ 'mail' :  req.query.email });
    if (req.session.user) {

        res.redirect('/home');

    } else {
        res.render('otp.ejs', {
            error : req.flash("error"),
            success: req.flash("success"),
            session:req.session
        });
    }
}
    
exports.accActive = function(req, res) {
    user.select('mail otp');
    user.exec(function (err, person) {
        if (err) return handleError(err);
        if (req.body.otp == person.otp) {
            person.status = 'active';
            req.flash('success', 'Đã kích hoạt tài khoản thành công, bạn đã có thể đăng nhập được.');
            res.redirect('/login');
        } else {
            req.flash('error', 'Mã kích hoạt đã nhập không chính xác.');
            res.render('otp.ejs', {
                error : req.flash("error"),
                success: req.flash("success"),
                session:req.session
            });
        }
        person.save();
    });
}

exports.admin = function(req, res) {

    var categoryPage = 1;
    if (req.query.categoryPage) {
        categoryPage = req.query.categoryPage;
    }
    var userPage = 1;
    if (req.query.userPage) {
        userPage = req.query.userPage;
    }
    var productPage = 1;
    if (req.query.productPage) {
        productPage = req.query.productPage;
    }

    var CategoryObjects = Category.find({}).populate('parent');
    var CategoryShowObjects = Category.find({}).populate('parent').limit(10).skip(10*(categoryPage-1));
    var ProductObjects = Product.find({}).populate('seller');
    var ProductShowObjects = Product.find({}).populate('seller').limit(5).skip(5*(productPage-1));
    var UserObjects = User.find({});
    var UserShowObjects = User.find({}).limit(5).skip(5*(userPage-1));
    var ThisUserObject = User.findOne({ _id : req.session.user});

    var resources = {
        categories: CategoryObjects.exec.bind(CategoryObjects),
        categoryPage: function(callback) {callback(null,categoryPage);},
        categoriesShow: CategoryShowObjects.exec.bind(CategoryShowObjects),
        products: ProductObjects.exec.bind(ProductObjects),
        productPage: function(callback) {callback(null,productPage);},
        productsShow: ProductShowObjects.exec.bind(ProductShowObjects),
        users: UserObjects.exec.bind(UserObjects),
        userPage: function(callback) {callback(null,userPage);},
        usersShow: UserShowObjects.exec.bind(UserShowObjects),
        thisUser: ThisUserObject.exec.bind(ThisUserObject),
        moment: function(callback) {callback(null,moment);}
    };

    async.parallel(resources, function (error, results){
        if (error) {
            res.status(500).send(error);
            return;
        }
        res.render('admin.ejs',results);
    });
}


exports.userProfile = function(req, res) {
    var CategoryObjects = Category.find({}).populate('parent');
    var ThisUserObject = User.findOne({ _id : req.session.user}).populate('favoriteProducts').populate('productsWon').populate('productsBid');

    var resources = {
        categories: CategoryObjects.exec.bind(CategoryObjects),
        thisUser: ThisUserObject.exec.bind(ThisUserObject),
        moment: function(callback) {callback(null,moment);}
    };

    async.parallel(resources, function (error, results){
        if (error) {
            res.status(500).send(error);
            return;
        }
        res.render('userProfile.ejs',results);
    });
}



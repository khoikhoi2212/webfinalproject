var User = require('../models/user');
var Product = require('../models/product');

exports.addUser = function(req, res) {
    var newUser = new User();

    newUser.fullname = req.body.name;
    newUser.dayOfBirth = req.body.dayOfBirth;
    newUser.mail = req.body.email;
    newUser.password = req.body.password;
    newUser.role = req.body.role;

    newUser.save();
    res.redirect('/adminPage');
}

exports.deleteUser = function(req, res) {
    User.deleteOne({ _id: req.query.id }, function (err) {});
    res.redirect('/adminPage');
}

exports.editUser = function(req, res) {

    User.findById(req.query.id , function (err, doc) {
        doc.fullname = req.body.name;
        doc.dayOfBirth = req.body.father;
        doc.mail = req.body.email;
        doc.password = doc.generateHash(req.body.password);
        doc.role = req.body.role;
        doc.save();
    });
    res.redirect('/adminPage');
}

exports.editProfile = function(req, res) {

    User.findById(req.session.user._id , function (err, doc) {
        if (!doc.validPassword(req.body.password)) {
            res.redirect('/profile');
        }
        else {
            doc.fullname = req.body.name;
            doc.mail = req.body.email;
            doc.password = doc.generateHash(req.body.newPassword);
            doc.save();
            res.redirect('/profile');
        }
    });
}

exports.addProductToFavorite = function(req, res) {

    User.findById(req.session.user._id , function (err, user) {
        user.favoriteProducts.push(req.query.productId);
        user.save();
        res.redirect('/profile');
    });
}
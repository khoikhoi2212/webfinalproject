var home = require('../app/controllers/home');
var category = require('../app/controllers/category');
var product = require('../app/controllers/product');
var user = require('../app/controllers/user');
var multipart = require('connect-multiparty');
var multipartMiddleware = multipart();

//you can include all your controllers

module.exports = function (app, passport) {

    app.get('/login', home.login);
    app.get('/signup', home.signup);
    app.get('/logout', home.logout);
    app.get('/otp', home.otp);
    app.get('/', home.home);//home
    app.get('/home', home.home);//home
    app.get('/adminPage',  home.admin);
    app.get('/profile', home.userProfile);
    app.get('/newProduct', product.productEditPage);
    app.get('/product', product.productDetailPage);
    app.get('/category/:id', product.productListByCategory);
    app.get('/search', product.searchProduct);
    app.get('/productList', product.productList);

    app.post('/signup', function(req, res, next) { 
        passport.authenticate('local-signup', {
            successRedirect: '/otp?email=' + req.body.email, // redirect to the secure profile section
            failureRedirect: '/signup', // redirect back to the signup page if there is an error
            failureFlash: true // allow flash messages
        })(req, res, next);
    });
    // process the login form
    app.post('/login', passport.authenticate('local-login', {
        successRedirect: '/home', // redirect to the secure profile section
        failureRedirect: '/login', // redirect back to the signup page if there is an error
        failureFlash: true // allow flash messages
    }));
    app.post('/accountActive', home.accActive);  
    app.post('/addCategory', category.addCategory);
    app.post('/editCategory', category.editCategory);
    app.get('/deleteCategory', category.deleteCategory);
    app.post('/addUser', user.addUser);
    app.post('/editUser', user.editUser);
    app.get('/deleteUser', user.deleteUser);
    app.get('/deleteProduct', product.deleteProduct);
    app.post('/editProfile', user.editProfile);
    app.post('/bid', product.bidProduct);
    app.post('/purchase', product.purchase);
    app.get('/addToFave', user.addProductToFavorite)
    
    app.post('/upload', multipartMiddleware, product.uploadCKeditor);
    app.post('/postProduct', multipartMiddleware, product.postProduct);
        
}

var LocalStrategy   = require('passport-local').Strategy;

var User            = require('../app/models/user');

var bcrypt = require('bcrypt-nodejs');
var request = require('request');

var configAuth = require('./auth.js');
var constant = require('../config/constants');
var dateFormat = require('dateformat');
var fs = require('fs');

var bcrypt = require('bcrypt-nodejs');


//expose this function to our app using module.exports
module.exports = function(passport) {

    // =========================================================================
    // passport session setup ==================================================
    // =========================================================================
    // required for persistent login sessions
    // passport needs ability to serialize and unserialize users out of session

    // used to serialize the user for the session
    passport.serializeUser(function(user, done) {
        done(null, user);
    });

    // used to deserialize the user
    passport.deserializeUser(function(id, done) {
        User.findById(id, function(err, user) {
            done(err, user);
        });
    });

    // =========================================================================
    // LOCAL SIGNUP ============================================================
    // =========================================================================
    // we are using named strategies since we have one for login and one for signup
    // by default, if there was no name, it would just be called 'local'

    passport.use('local-signup', new LocalStrategy({
        // by default, local strategy uses username and password, we will override with email
        usernameField : 'email',
        passwordField : 'password',
        passReqToCallback : true // allows us to pass back the entire request to the callback
    },
    function(req, email, password, done) {
        // asynchronous
        // User.findOne wont fire unless data is sent back
        process.nextTick(function() {
        // g-recaptcha-response is the key that browser will generate upon form submit.
        // if its blank or null means user has not selected the captcha, so return the error.
        if(req.body['g-recaptcha-response'] === undefined || req.body['g-recaptcha-response'] === '' || req.body['g-recaptcha-response'] === null) {
            return done(null, false, req.flash('error', 'Please select captcha .'));
        }
        // Put your secret key here.
        var secretKey = "6LdW_sgUAAAAACgYyaL4HnsqSpVW6_JL5xHxdwpG";
        // req.connection.remoteAddress will provide IP address of connected user.
        var verificationUrl = "https://www.google.com/recaptcha/api/siteverify?secret=" + secretKey + "&response=" + req.body['g-recaptcha-response'] + "&remoteip=" + req.connection.remoteAddress;
        // Hitting GET request to the URL, Google will respond with success or error scenario.
        request(verificationUrl,function(error,response,body) {
            body = JSON.parse(body);
            // Success will be true or false depending upon captcha validation.
            if(body.success !== undefined && !body.success) {
                return done(null, false, req.flash('error', 'Failed captcha verification .'));
            }
        });
        // find a user whose email is the same as the forms email
        // we are checking to see if the user trying to login already exists
        User.findOne({ 'mail' :  email }, function(err, user) {
            // if there are any errors, return the error
            if (err)
                return done(err);

            // check to see if theres already a user with that email
            if (user) {
                return done(null, false, req.flash('error', 'Email này đã có người sử dụng.'));
            } else if (password != req.body.password_repeat) {
                return done(null, false, req.flash('error', 'Xác nhận mật khẩu không trùng khớp.'));
            } else {
                
                
           User.find().sort([['_id', 'descending']]).limit(1).exec(function(err, userdata) {    

               
                // if there is no user with that email
                // create the user
                var newUser            = new User();

                // set the user's local credentials
             
                var otp = Math.floor((Math.random() * 9000) + 1000);
                    newUser.mail    = email;
                    newUser.password = newUser.generateHash(password);
                    newUser.fullname = req.body.fullname;
                    newUser.address = req.body.address;
                    newUser.otp = otp;
                    newUser.status = 'inactive'; //inactive for email actiavators

                // save the user
                newUser.save(function(err) {
                    if (err)
                        throw err;

                    var email = require('../lib/email.js');
                    email.activate_email(req.body.email,otp);

                    return done(null, newUser,req.flash('success', 'Đăng ký tài khoản thành công, kiểm tra email để xác nhận tài khoản'));
                    
                    req.session.destroy();
                
                });
                
              });
           
                
            }

        });    

        });

        
    }));
    
    
    // =========================================================================
    // LOCAL LOGIN =============================================================
    // =========================================================================
    // we are using named strategies since we have one for login and one for signup
    // by default, if there was no name, it would just be called 'local'
    
    passport.use('local-login', new LocalStrategy({
        // by default, local strategy uses username and password, we will override with email
        usernameField : 'email',
        passwordField : 'password',
        passReqToCallback : true // allows us to pass back the entire request to the callback
    },
    function(req, email, password, done) { // callback with email and password from our form

        
        
        // find a user whose email is the same as the forms email
        // we are checking to see if the user trying to login already exists
        User.findOne({ 'mail' :  email }, function(err, user) {
            // if there are any errors, return the error before anything else
            
            if (err)
            return done(null, false, req.flash('error', err)); // req.flash is the way to set flashdata using connect-flash


            // if no user is found, return the message
            if (!user)
                return done(null, false, req.flash('error', 'Tài khoản này không tồn tại, vui lòng đăng ký tài khoản.')); // req.flash is the way to set flashdata using connect-flash

            
            
            // if the user is found but the password is wrong
            if (!user.validPassword(password))
                return done(null, false, req.flash('error', 'Email và mật khẩu không trùng khớp.')); // create the loginMessage and save it to session as flashdata

            if(user.status === 'inactive')
             return done(null, false, req.flash('error', 'Tài khoản chưa được kích hoạt, kiểm tra email đề kích hoạt tài khoản')); // create the loginMessage and save it to session as flashdata
            
            
            // all is well, return successful user
            req.session.user = user;
        
            return done(null, user);
        });

    }));

};